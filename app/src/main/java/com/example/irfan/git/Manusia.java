package com.example.irfan.git;

public class Manusia {
    private String nama;
    private int umur;
    private String alamat;

    public Manusia() {

    }

    public Manusia(String nama, int umur, String alamat) {
        setNama(nama);
        setUmur(umur);
        setAlamat(alamat);
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String tampil() {
        return "Nama : " + nama + "\n"
                + "Umur : " + umur + "\n"
                + "Alamat : " + alamat;
    }
}
