package com.example.irfan.git;

public class Karyawan extends Manusia {
    private int nip;
    private int lama_kerja;
    private int gaji;
    private int bonus;
    private int tanggungan;

    public int getNip() {
        return nip;
    }

    public void setNip(int nip) {
        this.nip = nip;
    }

    public int getLama_kerja() {
        return lama_kerja;
    }

    public void setLama_kerja(int lama_kerja) {
        this.lama_kerja = lama_kerja;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public void asd ()
    {

    }

    public int getTanggungan() {
        return tanggungan;
    }

    public void setTanggungan(int tanggungan) {
        this.tanggungan = tanggungan;
    }

    public void asdsa()
    {

    }

    public void a11231231sdsa()
    {

    }

    public void a11231231sdsa2131231231()
    {

    }

    @Override
    public String tampil() {
        return "Nama : " + getNama() + "\n"
                + "Umur : " + getUmur() + "\n"
                + "Alamat : " + getAlamat() + "\n"
                + "NIP : " + nip + "\n"
                + "Lama Kerja : " + lama_kerja + "\n"
                + "Bonus : " + bonus;
    }
}
